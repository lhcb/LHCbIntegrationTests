Validation of Sprucing-DaVinci chain for luminosity FSR
------------------------------------------------------------

Project to validate the workflow : `Moore Sprucing on 2022 data - DaVinci typical selection - Tupling with the FSR TTree creation - Validation of the number of Lumi Events saved in the TTree`. It consists of different tests,
each one dedicated to test a specific step of the chain.
All the test commands are reported in the `FSR_lumi/CMakeLists.txt` file.

The test chain consists of the following steps:


1) Run offline processing processing after the HLT2 step output:
      * **Function**: [`Hlt2Conf.Sprucing_production:excl_spruce_production`](https://gitlab.cern.ch/lhcb/Moore/-/blob/master/Hlt/Hlt2Conf/python/Hlt2Conf/Sprucing_production.py#L55)
      * **Option file**: [`FSR_lumi/option_moore_spruce_2022_data.yaml`]
2) Run DaVinci and create a tuple from the Spruce step output
      * **Function**: [`DaVinciExamples.tupling.option_davinci_tupling_with_lumi_from_spruce:main`](https://gitlab.cern.ch/lhcb/DaVinci/-/blob/egraveri_LumiFSRtoTTree/DaVinciExamples/python/DaVinciExamples/tupling/option_davinci_tupling_with_lumi_from_spruce.py)
      * **Option file**: [`FSR_lumi/option_davinci_tupling_from_spruce_lumi.yaml`]
3) Validate FSR TTree by matching the number of Lumi events per Run with the numbers in the DST produced by the Sprucing
      * **Function**: [`Utilities/validator_tuple_FSR.py`]