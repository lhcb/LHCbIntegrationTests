Validation of the Moore-DaVinci chain with veloSP configuration
---------------------------------------------------------------

Project to validate the `Moore-DaVinci-Tupling` workflow when using the veloSP sequence, developed for MC samples without Retina clusters. It consists of different tests,
each one dedicated to test a specific step of the chain.
All the test commands are reported in the `Tupling_veloSP/CMakeLists.txt` file.
The test chain consists of a dedicated HLT1 step while all the other are the same as the ones run in 
the Tupling_default test chain.

The test chain on simulation import the global [`Utilities/jobMC_options.yaml`](https://gitlab.cern.ch/lhcb/LHCbIntegrationTests/-/blob/master/Utilities/jobMC_options.yaml) configuration and consists of the following steps:

1) Start the test chain running HLT1 Allen on a MC .digi file from the TestFileDB.
      * **Function**:    [`Moore.production:hlt1`](https://gitlab.cern.ch/lhcb/Moore/-/blob/master/Hlt/Moore/python/Moore/production.py)
      * **Option file**: [`Tupling_veloSP/option_moore_hlt1_allen.yaml`](https://gitlab.cern.ch/lhcb/LHCbIntegrationTests/-/blob/master/Tupling_veloSP/option_moore_hlt1_allen.yaml)
      * **Extra option**: hlt1_pp_veloSP
2) Run the HLT2 on the HLT1 step output, testing the MC workflow.
      * **Function**:    [`Moore.production:hlt2`](https://gitlab.cern.ch/lhcb/Moore/-/blob/master/Hlt/Moore/python/Moore/production.py)
      * **Option file**: [`Tupling_default/option_moore_hlt2_all_lines.yaml`](https://gitlab.cern.ch/lhcb/LHCbIntegrationTests/-/blob/master/Tupling_default/option_moore_hlt2_all_lines.yaml)
3) Run offline processing on the HLT2 step output with:
      - a Passthrough line 
            * **Function**:  [`Hlt2Conf.Sprucing_tests:spruce_passthrough`](https://gitlab.cern.ch/lhcb/Moore/-/blob/master/Hlt/Hlt2Conf/python/Hlt2Conf/Sprucing_tests.py)
            * **Option file**: [`Tupling_default/option_moore_passthrough.yaml`](https://gitlab.cern.ch/lhcb/LHCbIntegrationTests/-/blob/master/Tupling_default/option_moore_passthrough.yaml)
      - a Sprucing line
            * **Function**:  [`Hlt2Conf.Sprucing_tests:spruce_all_lines_realtime`](https://gitlab.cern.ch/lhcb/Moore/-/blob/master/Hlt/Hlt2Conf/python/Hlt2Conf/Sprucing_tests.py)
            * **Option file**: [`Tupling_default/option_moore_spruce_all_lines.yaml`](https://gitlab.cern.ch/lhcb/LHCbIntegrationTests/-/blob/master/Tupling_default/option_moore_spruce.yaml)
4) Run DaVinci and create a tuple from:
      - the HLT2 step output
            * **Function**: [`DaVinciTests.options_lhcbintegration_mc_excl_line:main`](https://gitlab.cern.ch/lhcb/DaVinci/-/blob/master/DaVinciTests/python/DaVinciTests/options_lhcbintegration_mc_excl_line.py)
            * **Option file**: [`Tupling_default/option_davinci_tupling_from_turbo.yaml`](https://gitlab.cern.ch/lhcb/LHCbIntegrationTests/-/blob/master/Tupling_default/option_davinci_tupling_from_turbo.yaml)
      - the Spruce step output
            * **Function**: [`DaVinciTests.options_lhcbintegration_mc_excl_line:main`](https://gitlab.cern.ch/lhcb/DaVinci/-/blob/master/DaVinciTests/python/DaVinciTests/options_lhcbintegration_mc_excl_line.py)
            * **Option file**: [`Tupling_default/option_davinci_tupling_from_spruce.yaml`](https://gitlab.cern.ch/lhcb/LHCbIntegrationTests/-/blob/master/Tupling_default/option_davinci_tupling_from_spruce.yaml)
