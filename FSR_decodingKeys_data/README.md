Test to check propagation and usage of the Decoding Keys in FileSummaryRecord (FSR)
-----------------------------------------------------------------------------------

Test to validate that the decoding keys from the data spruced sample are propagated correctly to the FileSummaryRecord (FSR).
The FSR is then used in the DaVinci to configure the decoding keys instead of the usual `input_manifest_file`.
All the test commands are reported in the `FSR_decodingKeys_data/CMakeLists.txt` file.

The test chain consists of the following steps:

1) Start the test chain running Spruce on data:
      * **Function**:    `Hlt2Conf.sprucing_settings.Sprucing_2022_1_production:excl_spruce_production`
      * **Option file**: `Utilities/jobFSR_options.yaml+FSR_decodingKeys_data/options_moore_spruce.yaml)`
2) Validate that Keys read from FSR produced in Spruce job are identical to `FSR_decodingKeys/expected_keys.json` which were obtained from running the exact same job locally
3) Run the DaVinci on the spruced output:
      * **Function**:    `DaVinciTests.option_davinci_tupling_with_lumi_from_spruce:main `
      * **Option file**: `Utilities/jobFSR_options.yaml+FSR_decodingKeys_data/options_davinci_tupling`
