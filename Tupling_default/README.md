Validation of Moore-DaVinci chain with default configuration
------------------------------------------------------------

Project to validate the `Moore-DaVinci-Tupling` workflow. It consists of different tests,
each one dedicated to test a specific step of the chain.
All the test commands are reported in the `Tupling_default/CMakeLists.txt` file.
The test chain starts with a MC .digi file available in the _TestFileDB_ and creates a `.root` file
containing a tuple produced with _FunTuple_.
The final tuple is created reading both a passthrough and a Sprucing `.dst` file.
Each step of the chain is run using the `Utilitites/logscript.sh` script that saves the corresponding _stdout_ and _stderr_.
The _stderr_ is then used for step validation by means of the `Utilities/validator.sh` script ensuring no error is
dumped in the _stderr_.
For the two final tuples an additional validation is performed using the `Utilities/validator_tuple.py` script checking that
the _DecayTree_ is not empty and contains exactly all the expected branches.

The test chain consists of the following steps:

1) Start the test chain running HLT1 Allen on a MC .digi file from the TestFileDB.
      * **Function**:    [`Moore.production:hlt1`](https://gitlab.cern.ch/lhcb/Moore/-/blob/master/Hlt/Moore/python/Moore/production.py)
      * **Option file**: [`Tupling_default/option_moore_hlt1_allen.yaml`](https://gitlab.cern.ch/lhcb/LHCbIntegrationTests/-/blob/master/Tupling_default/option_moore_hlt1_allen.yaml)
2) Run the HLT2 on the HLT1 step output, testing the MC workflow.
      * **Function**:    [`Moore.production:hlt2`](https://gitlab.cern.ch/lhcb/Moore/-/blob/master/Hlt/Moore/python/Moore/production.py)
      * **Option file**: [`Tupling_default/option_moore_hlt2_all_lines.yaml`](https://gitlab.cern.ch/lhcb/LHCbIntegrationTests/-/blob/master/Tupling_default/option_moore_hlt2_all_lines.yaml)
3) Run offline processing on the HLT2 step output with:
      - a Passthrough line
            * **Function**:  [`Hlt2Conf.Sprucing_tests:spruce_passthrough`](https://gitlab.cern.ch/lhcb/Moore/-/blob/master/Hlt/Hlt2Conf/python/Hlt2Conf/Sprucing_tests.py)
            * **Option file**: [`Tupling_default/option_moore_passthrough.yaml`](https://gitlab.cern.ch/lhcb/LHCbIntegrationTests/-/blob/master/Tupling_default/option_moore_passthrough.yaml)
      - a Sprucing line 
            * **Function**:  [`Hlt2Conf.Sprucing_tests:spruce_all_lines_realtime`](https://gitlab.cern.ch/lhcb/Moore/-/blob/master/Hlt/Hlt2Conf/python/Hlt2Conf/Sprucing_tests.py)
            * **Option file**: [`Tupling_default/option_moore_spruce_all_lines.yaml`](https://gitlab.cern.ch/lhcb/LHCbIntegrationTests/-/blob/master/Tupling_default/option_moore_spruce.yaml)
4) Run DaVinci and create a tuple from:
      - the HLT2 step output
            * **Function**: [`DaVinciTests.options_lhcbintegration_mc_excl_line:main`](https://gitlab.cern.ch/lhcb/DaVinci/-/blob/master/DaVinciTests/python/DaVinciTests/options_lhcbintegration_mc_excl_line.py)
            * **Option file**: [`Tupling_default/option_davinci_tupling_from_turbo.yaml`](https://gitlab.cern.ch/lhcb/LHCbIntegrationTests/-/blob/master/Tupling_default/option_davinci_tupling_from_turbo.yaml)
      - the Spruce step output
            * **Function**: [`DaVinciTests.options_lhcbintegration_mc_excl_line:main`](https://gitlab.cern.ch/lhcb/DaVinci/-/blob/master/DaVinciTests/python/DaVinciTests/options_lhcbintegration_mc_excl_line.py)
            * **Option file**: [`Tupling_default/option_davinci_tupling_from_spruce.yaml`](https://gitlab.cern.ch/lhcb/LHCbIntegrationTests/-/blob/master/Tupling_default/option_davinci_tupling_from_spruce.yaml)