###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import sys, os, glob, argparse
import ROOT
import json

FILENAMESDT = f"spruce_all_lines_production.b2oc.dst"
FILENAMEROOT = f"DV_example_lumi_sprucing_ntp.root"


def check_ttree_data(dst_file, root_file):
    # Load the JSON data
    if not os.path.isfile(dst_file):
        raise Exception("DST File: {dst_file} does not exist!")
    f = ROOT.TFile.Open(dst_file)
    json_data = json.loads(str(f.FileSummaryRecord))

    # Open the ROOT file and get the TTree
    if not os.path.isfile(root_file):
        raise Exception("ROOT File: {root_file} does not exist!")
    root_file = ROOT.TFile.Open(root_file)
    tree = root_file.Get("lumiTree")

    # Get the expected values from the JSON data
    expected_values = {}

    input_json = json_data

    for key, value in input_json['LumiCounter.eventsByRun']['counts'].items():
        expected_values[int(key)] = value
        print(
            f'//////////  DST  :   RUN     {key}   -     nlumiEv     {expected_values[int(key)]}'
        )

    # Check if the TTree data matches the expected values
    assert tree.GetEntries() > 0

    for i in range(tree.GetEntries()):
        tree.GetEntry(i)
        key = tree.runNumber
        sum_value = tree.sumLumievents
        print(
            f'**********  ROOT :   RUN     {key}   -     nlumiEv     {sum_value}'
        )
        if key not in expected_values or sum_value != expected_values[key]:
            return False

    return True


def main(FILENAMESDT, FILENAMEROOT):

    test_content = check_ttree_data(FILENAMESDT, FILENAMEROOT)
    if not test_content:
        print(
            "Problem with the TTree content: does not match FSR JSON content")
        raise Exception(
            "Problem with the TTree content: does not match FSR JSON content")
        return False
    else:
        print('CHECK VALIDATED')
    return True


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('DST', type=str, help='Pass DST file')
    parser.add_argument('ROOT', type=str, help='Pass ROOT file')
    args = parser.parse_args()

    main(args.DST, args.ROOT)
