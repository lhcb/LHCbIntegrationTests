###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import sys, os, glob, argparse
from ROOT import TFile


def main(process):
    list_vars = [
        "M", "P", "PT", "PX", "PY", "PZ", "ENERGY", "LOKI_NTRCKS_ABV_THRSHLD"
    ]
    particles = ["B", "daug1", "daug2"]

    vars_stored = []
    for part in particles:
        print(f"adding variable for part {part}")
        for var in list_vars:
            vars_stored.append("{}_{}".format(part, var))

    vars_stored.extend([
        'B_LOKI_daug1_PT', 'B_LOKI_daug2_PT', 'EVENTNUMBER', 'RUNNUMBER',
        'B_daug1_PID_K', 'B_daug2_PROBNN_K'
    ])

    #sort the expected vars
    vars_stored = sorted(vars_stored)

    #open the TFile and TTree
    ntuple = "dv_from_{}_ntuple.root".format(process.lower())
    if not os.path.isfile(ntuple):
        raise Exception("File: {} does not exist!".format(ntuple))
    infile = TFile.Open(ntuple)
    tree_dir = f"Tuple_{process.lower()}"  #if process == "Turbo" else "B0DsK_Tuple"
    intree = infile.Get('{}/DecayTree'.format(tree_dir))

    assert intree.GetEntries()
    #sort the stores vars
    names = sorted([b.GetName() for b in intree.GetListOfLeaves()])

    excluded_1 = set(vars_stored) - set(names)
    excluded_2 = set(names) - set(vars_stored)

    if len(excluded_1) != 0:
        raise Exception(
            'Number of stored variables is less than what is expected. The extra variables expected are: ',
            excluded_1)
    if len(excluded_2) != 0:
        raise Exception(
            'Number of stored variables is greater than what is expected. The extra variables stored are: ',
            excluded_2)

    infile.Close()


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        'process',
        type=str,
        help='Process can be "Spruce", "TurboPass" or "Hlt2"')
    args = parser.parse_args()

    main(args.process)
