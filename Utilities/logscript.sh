#!/bin/bash
###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
stdoutlog=$1
shift
stderrlog=$1
shift
"$@" > >(tee $stdoutlog) 2> >(tee $stderrlog >&2)
exit ${PIPESTATUS[0]}  # return the exit code of the command, not tee
