#!/usr/bin/env bash
###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# Takes a single argument, and returns exit code 0 if that file is empty and 1
# otherwise
# Expected use case is to verify that a file containing a processes's stderr is
# empty, indicating no failures

if [[ ! -f "$1" ]]; then
  echo "ERROR: File $1 does not exist:"; echo
  exit 1
fi

#count number of lines in sterr file, it should not be more than 2
lines=`wc -l $1 | cut -f1 -d' '`

#remove known warnings by hand
#https://gitlab.cern.ch/lhcb/LHCb/-/blob/master/GaudiConf/python/GaudiConf/QMTest/LHCbExclusions.py#L99
#OMP: Warning #96: Cannot form a team with 40 threads, using 1 instead.
#OMP: Hint Consider unsetting KMP_DEVICE_THREAD_LIMIT (KMP_ALL_THREADS), KMP_TEAMS_THREAD_LIMIT, and OMP_THREAD_LIMIT (if any are set).
file_wo_omp=`sed '/^OMP:/d' $1`

if [[ lines -ge 3 ]] && [[ -n "$file_wo_omp" ]]; then
    echo "ERROR: File $1 is not empty:"; echo
    cat "$1"
    exit 1  
else
  exit 0
fi

