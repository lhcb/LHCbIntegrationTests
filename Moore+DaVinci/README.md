Moore+DaVinci chain
-------------------

Simple test checking that both Moore and DaVinci application can be run correctly.
Similar chains can be created for other LHCb applications if they are included in any test of this package.