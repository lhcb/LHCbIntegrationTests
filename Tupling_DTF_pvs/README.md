Validation of Moore-DaVinci chain with default configuration for PV reconstruction
-----------------------------------------------------------------------------------

Project to validate the `Moore-DaVinci-Tupling` workflow for PV reconstruction. It consists of different tests,
each one dedicated to test a specific step of the chain.
All the test commands are reported in the `Tupling_DTF_pvs/CMakeLists.txt` file.
The test chain starts with a MC .digi file and creates a `.root` file
containing a tuple produced with _FunTuple_.
The final tuple is created reading a passthrough `.dst` file.
Each step of the chain is run using the `Utilitites/logscript.sh` script that saves the corresponding _stdout_ and _stderr_.
The _stderr_ is then used for step validation by means of the `Utilities/validator.sh` script ensuring no error is
dumped in the _stderr_.
For the final tuple an additional validation is performed using the `validator_tuple.py` script checking that the expected branches are consistent.

The test chain consists of the following steps:

1)  Run the HLT2 on digi input, testing the MC workflow.
      * **Function**:    [`Moore.production:hlt2`](https://gitlab.cern.ch/lhcb/Moore/-/blob/master/Hlt/Moore/python/Moore/production.py)
      * **Option file**: [`Tupling_DTF_pvs/option_moore_hlt2_all_lines.yaml`](https://gitlab.cern.ch/lhcb/LHCbIntegrationTests/-/blob/master/Tupling_DTF_pvs/option_moore_hlt2_all_lines.yaml)
3) Run offline processing on the HLT2 step output with:
      - a Passthrough line
            * **Function**:  [`Hlt2Conf.Sprucing_tests:spruce_passthrough`](https://gitlab.cern.ch/lhcb/Moore/-/blob/master/Hlt/Hlt2Conf/python/Hlt2Conf/Sprucing_tests.py)
            * **Option file**: [`Tupling_DTF_pvs/option_moore_passthrough.yaml`](https://gitlab.cern.ch/lhcb/LHCbIntegrationTests/-/blob/master/Tupling_DTF_pvs/option_moore_passthrough.yaml)
4) Run DaVinci and create a tuple from:
      - the HLT2 step output
            * **Function**: [`DaVinciExamples.tupling.option_davinci_Tupling_DTF_pvs:main`](https://gitlab.cern.ch/lhcb/DaVinci/-/blob/master/DaVinciExamples/python/DaVinciExamples/tupling/option_davinci_tupling_DTF_pvs.py?ref_type=heads)
            * **Option file**: [`Tupling_DTF_pvs/option_davinci_tupling_from_turbo.yaml`](https://gitlab.cern.ch/lhcb/LHCbIntegrationTests/-/blob/master/Tupling_DTF_pvs/option_davinci_tupling_from_turbo.yaml)
