###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# Adapted from https://gitlab.cern.ch/lhcb/LHCbIntegrationTests/-/blob/master/Utilities/validator_tuple.py
import sys, os, glob, argparse
from ROOT import TFile
from DaVinciTests.QMTest.check_helpers import get_pandas_dataframe, get_hash_from_branch


def main():

    filename = "dv_from_turbopass_ntuple.root"
    if not os.path.isfile(filename):
        raise Exception(f"File: {filename} does not exist!")
    ntuple = "JpsiPhi_Tuple/DecayTree"
    ntuple_unbiased = "JpsiPhiUnbiased_Tuple/DecayTree"

    df = get_pandas_dataframe(filename, ntuple)
    df_unbiased = get_pandas_dataframe(filename, ntuple_unbiased)
    vars = [
        '_CHI2', '_CHI2DOF', '_FD', '_FDERR', '_MASS', '_MASSERR', '_P',
        '_PERR', '_PV_X', '_PV_Y', '_PV_Z', '_CTAU', '_CTAUERR'
    ]

    #compare own pv with best pv: they should be identical
    for v in vars:
        mean_best = df["Bs_DTF_BestPV" + v].mean()
        mean_own = df["Bs_DTF_OwnPV" + v].mean()
        if round(mean_best - mean_own, 5) > 0.0:
            raise Exception(
                f"The mean for the var {v} is different between BestPV and OwnPV : {mean_best:.5f}, exp: {mean_own:.5f}"
            )

    #compare own pv with unbiased pv: they should be different
    for v in vars:
        mean_own = df["Bs_DTF_OwnPV" + v].mean()
        mean_unbiased = df_unbiased["Bs_DTF_UnbiasedPV" + v].mean()
        print(mean_own, mean_unbiased,
              round(abs(mean_unbiased - mean_own) / abs(mean_own), 5))
        if abs(mean_unbiased - mean_own) == 0.0:
            raise Exception(
                f"The mean for the var {v} is same between UnbiasedPV and OwnPV : {mean_unbiased:.5f}, exp: {mean_own:.5f}"
            )


if __name__ == "__main__":
    main()
