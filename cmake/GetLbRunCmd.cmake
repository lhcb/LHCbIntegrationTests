###############################################################################
# (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
if(NOT lbrun_cmd)
  find_program(lbrun_cmd_path lb-run)
  if(NOT lbrun_cmd_path)
    message(FATAL_ERROR "Cannot find the lb-run command, check your environment")
  endif()

  set(lbrun_cmd ${lbrun_cmd_path} --no-user-area)
  foreach(dir ${dev_dirs})
    set(lbrun_cmd ${lbrun_cmd} --dev-dir ${dir})
  endforeach()
endif()
