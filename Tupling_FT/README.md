Test for availability of Flavour Tagging variables
--------------------------------------------------

This integration test aims to determine if the flavour tagging variables are available in the ntuples.
The input is Upgrade Bd->DPi simulation in `xdigi` format (used in [B2DX early measurements](https://gitlab.cern.ch/lhcb-b2oc/analyses/b2dx-early-measurements/-/blob/master/options/Hlt1/xdigi_input_Bd2DPi_down.py)).
Afterwards, a Moore job runs HLT1 and saves the result as a `dst` file. Next, Moore is used to apply an exclusive flavour tagging HLT2 line, `BdToDmPi_DmToPimPimKp`, saving all long and upstream particles as `extra_outputs` and saving it to a `dst` file.
Finally, Davinci is run to produce a `root` file from this and it is checked if the resulting nTuple contains the flavour tagging variables and the expected values and number of events.
Each step of the chain is run using the `Utilitites/logscript.sh` script that saves the corresponding _stdout_ and _stderr_.
The _stderr_ is then used for step validation by means of the `Utilities/validator.sh` script ensuring no error is
dumped in the _stderr_.
For the final ntuple, an additional validation is performed using the `Tupling_FT/validator_tuple_FT.py` script, checking that
the _DecayTree_ is not empty and contains all the expected flavour tagging branches.


The test chain consists of the following steps:

1. Start test chain on Bd->DPi MC `.xdigi` file alse used in [B2DX early measurements](https://gitlab.cern.ch/lhcb-b2oc/analyses/b2dx-early-measurements/-/blob/master/options/Hlt1/xdigi_input_Bd2DPi_down.py)
      - **Function:**   [`Moore.production:hlt1`](https://gitlab.cern.ch/lhcb/Moore/-/blob/master/Hlt/Moore/python/Moore/production.py)
      - **Option file:**: `Tupling_FT/option_moore_hlt1_allen.yaml`

2. Run HLT2 on the HLT1 output
      - **Function**:   [`Moore.tests.lhcbintegrationtests_options_tupling_ft:hlt2_ft`](https://gitlab.cern.ch/lhcb/Moore/-/blob/master/Hlt/Moore/python/Moore/tests/lhcbintegrationtests_options_tupling_ft.py)
      - **Option file:** `Tupling_FT/option_moore_hlt2.yaml`

3. Run DaVinci on the HLT2 output and create an ntuple:
      - **Function:** [`DaVinciTests.options_lhcbintegration_mc_ft_bd2dpi:main`](https://gitlab.cern.ch/lhcb/DaVinci/-/blob/master/DaVinciTests/python/DaVinciTests/options_lhcbintegration_mc_ft_bd2dpi.py)
      - **Option file** `Tupling_FT/option_davinci_tupling_from_hlt2.yaml`

4. Test the resulting root file
      - **Python file** `Tupling_FT/validator_tuple_FT.py`
      - **json file** `Tupling_FT/validator_tuple_FT.json`
