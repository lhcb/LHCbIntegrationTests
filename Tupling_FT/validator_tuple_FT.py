###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# Adapted from https://gitlab.cern.ch/lhcb/LHCbIntegrationTests/-/blob/master/Utilities/validator_tuple.py
import os, argparse
from DaVinciTests.QMTest.check_helpers import get_pandas_dataframe
import json
from pytest import approx

parser = argparse.ArgumentParser()
parser.add_argument(
    '--jsonFile', help='Input json file to extract information from')


def main(jsonFile):

    # Open the ROOT file in pandas dataframe
    ntuple = "daVinci_Bd2DPi_ntuple.root"
    # Check: does the file exist
    if not os.path.isfile(ntuple):
        raise Exception(f"File: {ntuple} does not exist!")
    df = get_pandas_dataframe(ntuple, f"Tuple/DecayTree")

    # Check: does the input json file exist
    if not os.path.isfile(jsonFile):
        raise Exception(f"File: {jsonFile} does not exist!")

    # Open json file with properties of nTuple
    with open(jsonFile, 'r') as json_file:
        prop_dict = json.load(json_file)

    # Check if there are entries
    if df.empty:
        raise Exception(f"File {ntuple}: ntuple does not contain any entries")

    # Check the number of rows and columns
    expected_shape = tuple(prop_dict['shape'])
    if df.shape != expected_shape:
        raise Exception(
            f"Dataframe does not have the correct shape\n   df: {df.shape}, exp: {expected_shape}"
        )

    # Check if there are any nan values
    N_nan = df.isnull().values.sum()
    if N_nan > 0:
        raise Exception(f"Dataframe contains {N_nan} NaN values")

    # Get the list of vars
    vars_expected = sorted(prop_dict['vars_expected'])
    vars_df = sorted(list(df.columns))
    print(f"Expected list of variables:\n   {vars_expected}")
    print(f"Found list of variables:\n   {vars_df}")

    # Check if list of variables are different
    excluded_1 = set(vars_expected) - set(vars_df)
    excluded_2 = set(vars_df) - set(vars_expected)
    if len(excluded_1) != 0:
        raise Exception(
            "Number of stored variables is less than what is expected. The extra variables expected are: ",
            excluded_1,
        )
    if len(excluded_2) != 0:
        raise Exception(
            "Number of stored variables is greater than what is expected. The extra variables stored are: ",
            excluded_2,
        )

    ok = True

    # Check if the mean of the FT vars is as expected
    meanDict = prop_dict["mean"]
    for var in meanDict:
        expect = meanDict[var]
        found = df[var].mean()
        if expect != approx(found, abs=1e-2):
            print(
                f"The mean of var {var} is different than expected\n   df: {found:.5f}, exp: {expect:.5f}"
            )
            ok = False

    # Check if the std of the FT vars is as expected
    stdDict = prop_dict["std"]
    for var in stdDict:
        expect = stdDict[var]
        found = df[var].std()
        if expect != approx(found, rel=1e-2):
            print(
                f"The standard deviation of var {var} is different than expected\n   df: {found:.5f}, exp: {expect:.5f}"
            )
            ok = False

    if not ok:
        raise Exception("NTuple FAILED checks")

    print(" --- nTuple passed all tests ---")


if __name__ == "__main__":
    try:
        args = parser.parse_args()
    except:
        parser.print_help()
        sys.exit(-1)

    main(args.jsonFile)
