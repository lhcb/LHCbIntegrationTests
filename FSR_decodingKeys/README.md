Test to check propagation of the Decoding Keys and Application options to FileSummaryRecord (FSR)
-----------------------------------------------------------------------------------------------------

Test to validate that the decoding keys, from both the HLT2 and Sprucing stages as well as the application options used to run the Sprucing, are propagated correctly to the FileSummaryRecord (FSR).
The test chain starts with running Hlt2 on a (dd4hep compatible) MC Bs2JPsiPhi xdigi file.
All the test commands are reported in the `FSR_decodingKeys/CMakeLists.txt` file.

The test chain consists of the following steps:

1) Start the test chain running HLT2 on data:
      * **Function**:    `Moore.tests.lhcbintegrationtests_options_decodingkeys`
      * **Option file**: `FSR_decodingKeys/options_moore_hlt2.yaml`
2) Run the Sprucing on HLT2 output:.
      * **Function**:    `Hlt2Conf.Sprucing_tests:spruce_test_lines`
      * **Option file**: `FSR_decodingKeys/options_moore_spruce.yaml`
3) Validate that the Keys read from FSR produced in the Spruce job are identical to `FSR_decodingKeys/expected.json` which were obtained from running the exact same job locally 
4) Validate that the Options read from FSR produced in the Spruce job are identical to `FSR_decodingKeys/expected.json`, which were obtained from running the exact same job locally and correspond to the values set in `FSR_decodingKeys/options_moore_spruce.yaml` plus default values that are not set explicitly in the yaml.
