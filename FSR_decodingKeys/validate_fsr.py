###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os, argparse
import ROOT
import json

FILENAME = f"spruce_test_lines_production.dst"
EXPECTEDKEYSNAME = f"expected.json"


def check_keys(dst_file, expected_file, item):
    # Load the JSON data
    if not os.path.isfile(dst_file):
        raise Exception(f"DST File: {dst_file} does not exist!")
    if not os.path.isfile(expected_file):
        raise Exception(f"Expected json: {expected_file} does not exist!")

    print(dst_file)
    with ROOT.TFile.Open(dst_file) as f_dst:
        fsr = json.loads(str(f_dst.FileSummaryRecord))
        print("Found these options in FSR: \n", json.dumps(fsr[item]))

        keys_fromFSR = fsr[item]

    with open(expected_file, 'r') as f_json:
        print("Successfully opened ", expected_file)
        expected_keys = json.load(f_json)[item]

    if expected_keys != keys_fromFSR:
        return False

    return True


def main(FILENAME, EXPECTEDKEYSNAME, ITEM):

    test_outcome = check_keys(FILENAME, EXPECTEDKEYSNAME, ITEM)
    if not test_outcome:
        print(f"Problem with the {ITEM}: do not match FSR JSON content")
        raise Exception(
            f"Problem with the {ITEM}: do not match FSR JSON content")
    else:
        print('CHECK VALIDATED')
    return True


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('DST', type=str, help='Pass DST file')
    parser.add_argument('JSON', type=str, help='Pass JSON file')
    parser.add_argument(
        'ITEM',
        type=str,
        help=
        'Pass item to be validated. Should be one of the keys in the FSR e.g. "application_options".'
    )
    args = parser.parse_args()

    main(args.DST, args.JSON, args.ITEM)
