Test for neutrals
-----------------

This integration test aims to determine if the neutral info in proto particles are stored correctly in the ntuples, using the `Hlt2RD_BToEEGamma` line.

The test chain consists of the following steps:

1. Run HLT2 on the HLT1 input
    - **Function**:   [`Moore.tests.lhcbintegrationtests_options_neutralst:neutrals`](https://gitlab.cern.ch/lhcb/Moore/-/blob/master/Hlt/Moore/python/Moore/tests/lhcbintegrationtests_options_neutrals.py)
    - **Option file:** `Tupling_neutrals/option_moore_hlt2.yaml`

2. Run Passthrough on HLT2 output 
    - **Function**:   [`Hlt2Conf.Sprucing_tests:spruce_passthrough`](https://gitlab.cern.ch/lhcb/Moore/-/blob/master/Hlt/Hlt2Conf/python/Hlt2Conf/Sprucing_tests.py)
    - **Option file:** `Tupling_neutrals/option_moore_passthrough.yaml`

3. Run DaVinci on the Passthrough output and create an ntuple:
    - **Function:** [`DaVinciTests.options_lhcbintegration_neutrals:main`](https://gitlab.cern.ch/lhcb/DaVinci/-/blob/master/DaVinciTests/python/DaVinciTests/options_lhcbintegration_neutrals.py)
    - **Option file** `Tupling_neutrals/option_davinci_from_pass.yaml`

4. Test the resulting root file
    - **Python file** `Tupling_neutrals/variables_tuple.py`
