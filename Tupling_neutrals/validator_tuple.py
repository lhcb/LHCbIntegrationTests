###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# Adapted from https://gitlab.cern.ch/lhcb/LHCbIntegrationTests/-/blob/master/Utilities/validator_tuple.py
import sys, os, glob, argparse
from ROOT import TFile
from DaVinciTests.QMTest.check_helpers import has_nan, list_fields_with_nan, get_pandas_dataframe
from variables_tuple import *
from math import isnan


def main():
    # The expected values of the variables are in variables_tuple.py
    # In case, they need to be updated due to a change input file or reconstruction
    # or addition of new variables, uncomment the lines 68-69 to get the new reference values.

    vars_stored = {
        "gamma_ISPHOTON":
        gamma_ISPHOTON,
        "gamma_ISNOTH":
        gamma_ISNOTH,
        "gamma_CaloTrackMatchChi2":
        gamma_CaloTrackMatchChi2,
        "gamma_CaloNeutralShowerShape":
        gamma_CaloNeutralShowerShape,
        "gamma_CaloClusterMass":
        gamma_CaloClusterMass,
        "gamma_CaloNeutralEcalEnergy":
        gamma_CaloNeutralEcalEnergy,
        "gamma_CaloNeutral1To9EnergyRatio":
        gamma_CaloNeutral1To9EnergyRatio,
        "gamma_CaloNeutral4To9EnergyRatio":
        gamma_CaloNeutral4To9EnergyRatio,
        "gamma_CaloNeutralHcal2EcalEnergyRatio":
        gamma_CaloNeutralHcal2EcalEnergyRatio,
        "gamma_CaloNeutralID":
        gamma_CaloNeutralID,
        "gamma_CaloNumSaturatedCells":
        gamma_CaloNumSaturatedCells,
        "gamma_P":
        gamma_P,
        "gamma_TRUEP":
        gamma_TRUEP,
        "gamma_TRUEID":
        gamma_TRUEID,
        "gamma_MC_MOTHER_ID":
        gamma_MC_MOTHER_ID,
        "gamma_MC_MOTHER_KEY":
        gamma_MC_MOTHER_KEY,
        "Bs_BKGCAT":
        Bs_BKGCAT
    }

    filename = "B2eegamma.root"
    if not os.path.isfile(filename):
        raise Exception(f"File: {filename} does not exist!")
    ntuple = "Tuple/DecayTree"

    exception_message = ""
    df = get_pandas_dataframe(filename, ntuple)
    for k in vars_stored.keys():
        print(k, " = ", df[k].to_list())

    # Check ntuple structure
    if df.empty:
        exception_message += f"File {filename}: ntuple does not contain any branches.\n"

    if df.shape != (41, 76):
        exception_message += f"Ntuple not with expected number of entries and/or branches. Expected (37,76) but found {df.shape}.\n"

    # Check there are no NaN values in the ntuple except where expected
    l_branches_with_nans = [
        'Bs_TRUEENERGY', 'Bs_TRUEP', 'Bs_TRUEPT', 'Bs_TRUEPX', 'Bs_TRUEPY',
        'Bs_TRUEPZ', 'Jpsi_TRUEENERGY', 'Jpsi_TRUEP', 'Jpsi_TRUEPT',
        'Jpsi_TRUEPX', 'Jpsi_TRUEPY', 'Jpsi_TRUEPZ', 'gamma_M'
    ]

    l_test = list_fields_with_nan(filename, ntuple)
    if sorted(l_test) != sorted(l_branches_with_nans):
        exception_message += f"Unexpected list of branches with NaN values: Expected {l_branches_with_nans}, but found {l_test}.\n"

    failed = []
    for k, v in vars_stored.items():
        dfk = [x for x in df[k].to_list() if isnan(x) == False]
        v = [x for x in v if isnan(x) == False]

        if v != dfk:
            print("Expected: ")
            print(k, "= ", v)

            print("Found in the ntuple")
            print(k, "= ", dfk)
            failed.append(k)

    if len(failed) > 0:
        exception_message += f"Branch {failed} entries do not match the expected entries.\n"

    if exception_message:
        raise Exception(exception_message)


if __name__ == "__main__":
    main()
