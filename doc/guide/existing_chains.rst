Existing test chains
====================

Currently some test chains have been implemented testing the whole offline chain starting with the HLT1 step in Moore up to the production of the final user-ntuples generated with FunTuple in DaVinci.
Each test is repeated both on a data and a simulated sample to assert the correct behaviour in both cases.

You can issue the following command to run a whole test chain (e.g. `Tupling_default`) and generate the final `.root` files:

.. code-block:: bash

   run make test ARGS="-R LHCbIntegrationTests.Tupling_default"

Or, if you are working on an LHCb stack:

.. code-block:: bash

   make fast/LHCbIntegrationTests/test ARGS="-R Tupling_default"



.. include:: ../../Moore+DaVinci/README.md


.. include:: ../../Tupling_default/README.md


.. include:: ../../Tupling_veloSP/README.md


.. include:: ../../Tupling_FT/README.md


.. include:: ../../Tupling_neutrals/README.md


.. include:: ../../Tupling_DTF_pvs/README.md


.. include:: ../../Tupling_decodingKeys/README.md


.. include:: ../../Tupling_decodingKeys_data/README.md
