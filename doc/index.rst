Welcome to LHCbIntegrationTests's documentation!
================================================

The LHCb Integration Tests project is a simple shell used to host tests meant to
check the integration between LHCb software projects.
This site documents the various tests of LHCbIntegration tests, which are called directly in CMakeFile.txt, and provide
also a brief tuttorial about how to create a new test chain.

.. toctree::
   :caption: User Guide
   :maxdepth: 3

   guide/existing_chains
   guide/create_new_chain
   guide/documentation 

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
