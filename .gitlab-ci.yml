###############################################################################
# (c) Copyright 2000-2024 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
variables:
  TARGET_BRANCH: master

check-copyright:
  image: gitlab-registry.cern.ch/ci-tools/ci-worker:cc7
  script:
    - curl -o lb-check-copyright "https://gitlab.cern.ch/lhcb-core/LbDevTools/raw/master/LbDevTools/SourceTools.py?inline=false"
    - python lb-check-copyright origin/${TARGET_BRANCH} --exclude lhcbproject.yml

build-docs:
  image: gitlab-registry.cern.ch/lhcb-docker/os-base/alma9-devel:latest
  tags:
    - cvmfs
  # Change pip's cache directory so we can cache it
  variables:
    PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"
  script:
    - . /cvmfs/lhcb.cern.ch/lib/LbEnv.sh
    - reasons=
    - make -C doc linkcheck || reasons+='ERROR failed link check\n'
    - make -C doc html || reasons+='ERROR failed html generation\n'
    - if [ -n "$reasons" ]; then echo -e $reasons; exit 1; fi
  allow_failure:
    exit_codes: 77
  artifacts:
    paths:
      - doc/_build/html/
  cache:
    key: "$CI_JOB_NAME"
    paths:
      - .cache/pip

pages:
  needs: [build-docs]
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
  script:
   - mv doc/_build/html public
  artifacts:
    untracked: false
    expire_in: 30 days
    paths:
     - public

include:
  - project: 'lhcb-core/dev-tools/ci-utils'
    file: 'gitlab-ci-fragments/pre-commit-checks.yaml'
