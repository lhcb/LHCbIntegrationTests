###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# Test if track refitting variables are available for particles
get_project_version(Moore)
get_project_version(DaVinci)

set(src_dir ${CMAKE_CURRENT_SOURCE_DIR})
set(util_dir ${src_dir}/../Utilities/)

# Temporarily disable tests for dd4hep platforms
# as we want to include *all* detectors
# including the UT
string(FIND "$ENV{BINARY_TAG}" "detdesc" idx)
if(NOT idx EQUAL -1)
    # Run the HLT2 on HLT1 filtered bs2dspi data
    add_test(NAME Tupling_track_refitting.RunHLT2.Run
                COMMAND ${util_dir}/logscript.sh Tupling_track_refitting.RunHLT2.stdout.log Tupling_track_refitting.RunHLT2.stderr.log
                ${Moore_run} lbexec Moore.tests.lhcbintegrationtests_options_tupling_track_refitting:run_hlt2 ${src_dir}/jobMC_options.yaml+${src_dir}/option_moore_hlt2.yaml)
    set_property(TEST Tupling_track_refitting.RunHLT2.Run PROPERTY TIMEOUT 3600)
    # to-do: add main function to moore option file and run with lbexec and yaml file

    add_test(NAME Tupling_track_refitting.RunHLT2.Validate
            COMMAND ${util_dir}/validator.sh Tupling_track_refitting.RunHLT2.stderr.log)
    set_property(TEST Tupling_track_refitting.RunHLT2.Validate
                APPEND PROPERTY DEPENDS Tupling_track_refitting.RunHLT2.Run)

    # Run Moore for the sprucing to create a DST
    add_test(NAME Tupling_track_refitting.RunSprucing.Run
        COMMAND ${util_dir}/logscript.sh Tupling_track_refitting.RunSprucing.stdout.log Tupling_track_refitting.RunSprucing.stderr.log
        ${Moore_run} lbexec Hlt2Conf.Sprucing_tests:spruce_example_realtime_persistreco ${src_dir}/jobMC_options.yaml+${src_dir}/option_moore_spruce.yaml)

    set_property(TEST Tupling_track_refitting.RunSprucing.Run
                APPEND PROPERTY DEPENDS Tupling_track_refitting.RunHLT2.Run)
    set_property(TEST Tupling_track_refitting.RunSprucing.Run PROPERTY TIMEOUT 3600)
    add_test(NAME Tupling_track_refitting.RunSprucing.Validate
            COMMAND ${util_dir}/validator.sh Tupling_track_refitting.RunSprucing.stderr.log)
    set_property(TEST Tupling_track_refitting.RunSprucing.Validate
                APPEND PROPERTY DEPENDS Tupling_track_refitting.RunSprucing.Run)

    # Run DaVinci for the sprucing to create a tuple and refit the tracks
    add_test(NAME Tupling_track_refitting.RefitTrackAndTuple.Run
        COMMAND ${util_dir}/logscript.sh Tupling_track_refitting.RefitTrackAndTuple.stdout.log Tupling_track_refitting.RefitTrackAndTuple.stderr.log
        ${DaVinci_run} lbexec DaVinciTests.options_lhcbintegration_track_refitting:main ${src_dir}/jobMC_options.yaml+${src_dir}/option_davinci_tupling.yaml)

    set_property(TEST Tupling_track_refitting.RefitTrackAndTuple.Run
                APPEND PROPERTY DEPENDS Tupling_track_refitting.RunSprucing.Run)
    set_property(TEST Tupling_track_refitting.RefitTrackAndTuple.Run PROPERTY TIMEOUT 3600)

    add_test(NAME Tupling_track_refitting.RefitTrackAndTuple.Validate
            COMMAND ${util_dir}/validator.sh Tupling_track_refitting.RefitTrackAndTuple.stderr.log)
    set_property(TEST Tupling_track_refitting.RefitTrackAndTuple.Validate
                APPEND PROPERTY DEPENDS Tupling_track_refitting.RefitTrackAndTuple.Run)

    add_test(NAME Tupling_track_refitting.RefitTrackAndTuple.ValidateTuple
            COMMAND ${util_dir}/logscript.sh Tupling_track_refitting.RefitTrackAndTuple.ValidateTuple.stdout.log Tupling_track_refitting.RefitTrackAndTuple.ValidateTuple.stderr.log
            ${DaVinci_run} python ${src_dir}/validator_tuple_and_histo_refit.py)
    set_property(TEST Tupling_track_refitting.RefitTrackAndTuple.ValidateTuple
                APPEND PROPERTY DEPENDS Tupling_track_refitting.RefitTrackAndTuple.Run)
endif()
