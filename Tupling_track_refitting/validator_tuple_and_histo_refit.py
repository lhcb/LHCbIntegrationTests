###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os
import ROOT


def main():
    # Open the ROOT file in pandas dataframe
    ntuple = "passthrough_tuple.root"
    # Check: does the file exist
    if not os.path.isfile(ntuple):
        raise Exception(f"File: {ntuple} does not exist!")

    root_file = ROOT.TFile.Open(ntuple, "READONLY")
    decay_tree = root_file.Get("DecayTreeTuple/DecayTree")

    # Check if there are entries
    if decay_tree.GetEntries() == 0:
        raise Exception(f"File {ntuple}: ntuple does not contain any entries")

    # Check the number of rows and columns
    expected_branches = [
        "muminus_P", "muminus_ETA", "muminus_Refitted_WEIGHT", "nMatchTracks",
        "muminus_Refitted_P", "muminus_CHI2DOF", "muminus_Refitted_CHI2DOF"
    ]
    for expected_branch in expected_branches:
        if not hasattr(decay_tree, expected_branch):
            raise Exception(
                f"File {ntuple}: ntuple does not contain expected branch {expected_branch}"
            )

    # plot the differences
    decay_tree.Draw(
        "(muminus_P - muminus_Refitted_P[0])/muminus_P>>momentum_difference",
        "muminus_Refitted_WEIGHT[0]>0.99")
    momentum_difference = ROOT.gDirectory.Get("momentum_difference")
    momentum_difference.SetDirectory(0)

    decay_tree.Draw(
        "(muminus_CHI2DOF - muminus_Refitted_CHI2DOF[0])>>chi2_difference",
        "muminus_Refitted_WEIGHT[0]>0.99")
    chi2_difference = ROOT.gDirectory.Get("chi2_difference")
    chi2_difference.SetDirectory(0)

    decay_tree.Draw("(muminus_ETA - muminus_Refitted_ETA[0])>>eta_diff",
                    "muminus_Refitted_WEIGHT[0]>0.99")
    eta_difference = ROOT.gDirectory.Get("eta_diff")
    eta_difference.SetDirectory(0)

    decay_tree.Draw("muminus_Refitted_WEIGHT[0]>>weight_distribution")
    weight_distribution = ROOT.gDirectory.Get("weight_distribution")
    weight_distribution.SetDirectory(0)

    if weight_distribution.GetMean() < 0.99:
        raise Exception("Discrepancy in the outlier rejection")

    if momentum_difference.GetRMS() > 0.001:
        raise Exception("Too large momentum spread! {}".format(
            momentum_difference.GetRMS()))

    if abs(momentum_difference.GetMean()) > 0.0001:
        raise Exception("Too large momentum bias! {}".format(
            momentum_difference.GetMean()))

    if eta_difference.GetRMS() > 0.001:
        raise Exception("Too large eta spread! {}".format(
            eta_difference.GetRMS()))

    if abs(eta_difference.GetMean()) > 0.0001:
        raise Exception("Too large eta bias! {}".format(
            eta_difference.GetMean()))

    if chi2_difference.GetRMS() > 0.01:
        raise Exception("Too large chi2 spread! {}".format(
            chi2_difference.GetRMS()))

    if abs(chi2_difference.GetMean()) > 0.0001:
        raise Exception("Too large chi2 bias! {}".format(
            chi2_difference.GetMean()))

    root_file.Close()

    print(" --- nTuple passed all tests ---")

    histogram_file = "passthrough_histos.root"
    # Check: does the file exist
    if not os.path.isfile(histogram_file):
        raise Exception(f"File: {histogram_file} does not exist!")

    root_file = ROOT.TFile.Open(histogram_file, "READONLY")
    histogram_state_info = root_file.Get(
        "RefittedTrackCorrelationsMonitor/Long/TrackStateAtTUpX")

    if histogram_state_info.GetEntries() == 0:
        raise Exception(
            "No entries in the TrackCorrelationsMonitor for refitted tracks?")

    if abs(histogram_state_info.GetMean()) > 25:
        raise Exception("Refitted T state mean too high - biased sample?")

    root_file.Close()
    print(" --- TrackCorrelationsMonitor passed all tests ---")


if __name__ == "__main__":
    main()
